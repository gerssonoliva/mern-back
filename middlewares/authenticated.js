const jwt = require("../utils/jwt");

function asureAuth(req, res, next) {
  if(!req.headers.autorization){
    res.status(403).send({ msg: "La peticion no tiene la cabecera de autenticacion" });
  }

  const token = req.headers.autorization.replace("Bearer ", ""); // quitar la palabra Bearer del token

  try {
    const payload = jwt.decoded(token);

    const { exp } = payload;
    const currentData = new Date().getTime();
    
    if(exp <= currentData){
      res.status(400).send({ msg: "El token ha expirado" });
    }

    req.user = payload;

  } catch (error) {
    return res.status(400).send({ msg: "Token inválido" });
  }
  next(); // da luz verde para proceder con la funcion    
}

module.exports = {
  asureAuth
}