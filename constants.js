const DB_USER = "admin";
const DB_PASSWORD = "admin123456";
const DB_HOST = "web-personal.kyoxcxz.mongodb.net";

const API_VERSION = "v1";
const IP_SERVER = "localhost";

const JWT_SECRET_KEY = "bfiy2ger87643gt8i75d7776ggdfsdfgdfgdfhdfhghfgj";

module.exports = {
    DB_USER,
    DB_PASSWORD, 
    DB_HOST,
    API_VERSION,
    IP_SERVER,
    JWT_SECRET_KEY
}