const Course = require("../models/course");
const image = require("../utils/image");

async function createCourse(req, res) {
  const course = new Course(req.body);

  const imagePath = image.getFilePath(req.files.miniature);
  course.miniature = imagePath;

  course.save().then((courseStore, err)=>{
    res.status(201).send(courseStore);
  }).catch((err)=>{
      res.status(400).send({msg: "Error al crear el curso. "+err});
  })
}

async function getCourse(req, res){
  const { page=1, limit=10 } = req.query;
  const options = {
    page: page,
    limit: parseInt(limit),
  }

  Course.paginate({}, options, function(err, courses) {
    if(err){
      res.status(400).send({msg: "No se ha encontrado ningún curso. "+err});
    }else{
      res.status(200).send(courses);
    }
    
  });

}

async function updateCourse(req, res) {

  const { id } = req.params;
  const courseData = req.body;

  if(req.files.miniature){
    const imagePath = image.getFilePath(req.files.miniature);
    courseData.miniature = imagePath;
  }

  try {
    const cour = await Course.findOneAndUpdate({ _id: id }, courseData);
    if(cour){
      res.status(200).send({ msg: "Actualizacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al actualizar curso"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"});
  }
  
}

async function deleteCourse(req, res) {
  
  const { id } = req.params;

  try {
    const cour = await Course.findByIdAndDelete(id);
    if(cour){
      res.status(200).send({ msg: "Eliminacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al eliminar curso"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"});
  }

}

module.exports = {
  createCourse,
  getCourse,
  updateCourse,
  deleteCourse
}