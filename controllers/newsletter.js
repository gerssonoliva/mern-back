const Newsletter = require("../models/newsletter");

function suscribeEmail(req, res) {
	const { email } = req.body;

  if(!email)
    res.status(400).send({msg: "El email no puede ser vacío."});

	const newsletter = new Newsletter({
		email: email.toLowerCase()
	});

	newsletter.save().then((newsletterStore, err)=>{
	  res.status(200).send({msg: "Email ya registrado: "+newsletterStore.email});
	}).catch((err)=>{
		res.status(400).send({msg: "Error al registrar email. "+err});
	})
	
}

async function deleteNewsletter(req, res) {
  const { id } = req.params;
  
  try {
    const us = await Newsletter.findByIdAndDelete(id);
    if(us){
      res.status(200).send({ msg: "Eliminacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al eliminar email"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"});
  }
}

async function getEmails(req, res){
  const { page=1, limit=10 } = req.query;
  const options = {
    page: page,
    limit: parseInt(limit),
  }


  Newsletter.paginate({}, options, (err, emailsStored) => {
    if(err){
      res.status(400).send({msg: "No se ha encontrado ningún email. "+err});
    }else{
      res.status(200).send(emailsStored);
    }
    
  });

}

module.exports = {
  suscribeEmail,
  deleteNewsletter,
  getEmails
};