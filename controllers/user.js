const bcrypt = require("bcryptjs");
const User = require("../models/user");
const image = require("../utils/image");

async function getMe(req, res) {

  const { user_id } = req.user;
  const response = await User.findById(user_id);

  if(!response){
    res.status(400).send({msg: "NO SE HA ENCONTRADO USUARIO"});
  }else{
    res.status(200).send(response);
  }
  
}

async function getUsers(req, res) {

  const { active } = req.query;

  let response = null;

  if(active === undefined){
    response = await User.find();
  }else{
    response = await User.find({ active });
  }
  res.status(200).send(response);
}

async function createUser(req, res){
  const { password } = req.body;
  const user = new User({ ...req.body, active: false });

  const salt = bcrypt.genSaltSync(10);
  const hasPassword = bcrypt.hashSync(password, salt);
  user.password = hasPassword;

  if(req.files.avatar){
    const imagePath = image.getFilePath(req.files.avatar);
    user.avatar = imagePath;
  }

  user.save().then((userStore, err)=>{
    res.status(201).send(userStore);
  }).catch((err)=>{
    res.status(400).send({msg: "Error al crear el usuario. "+err});
  })

}

async function updateUser(req, res){
  const { id } = req.params;
  const userData = req.body;

  if(userData.password){
    const salt = bcrypt.genSaltSync(10);
    const hasPassword = bcrypt.hashSync(userData.password, salt);
    userData.password = hasPassword;
  }else{
    delete userData.password;
  }

  if(req.files.avatar){
    const imagePath = image.getFilePath(req.files.avatar);
    userData.avatar = imagePath;
  }
  /* await User.findByIdAndUpdate({_id: id}, userData, (err) => {
    console.log(err);
    if(err){
      res.status(400).send({ msg: "Error al actualizar usuario"});
    }else{
      res.status(200).send({ msg: "Actualizacion correcta"});
    }
  }); */

  try {
    const us = await User.findOneAndUpdate({ _id: id }, userData);
    if(us){
      res.status(200).send({ msg: "Actualizacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al actualizar usuario"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"});
  }
}

async function deleteUser(req, res) {
  const { id } = req.params;

  try {
    const us = await User.findByIdAndDelete(id);
    if(us){
      res.status(200).send({ msg: "Eliminacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al eliminar usuario"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"});
  }
}

module.exports = {
  getMe,
  getUsers,
  createUser,
  updateUser,
  deleteUser
}