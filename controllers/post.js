const Post = require("../models/post");
const image = require("../utils/image");

function createPost(req, res) {

  const post = new Post(req.body);
  post.created_at = new Date();

  const imagePath = image.getFilePath(req.files.miniature);
  post.miniature = imagePath;

  post.save().then((postStore, err)=>{
    res.status(201).send(postStore);
  }).catch((err)=>{
      res.status(400).send({msg: "Error al crear el blog. "+err});
  })

}

async function updatePost(req, res) {

  const { id } = req.params;
  const postData = req.body;

  if(req.files.miniature){
    const imagePath = image.getFilePath(req.files.miniature);
    postData.miniature = imagePath;
  }
    
  try {
    const cour = await Post.findOneAndUpdate({ _id: id }, postData);
    if(cour){
      res.status(200).send({ msg: "Actualizacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al actualizar post"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor: "+err});
  }

}

async function deletePost(req, res) {
  const { id } = req.params;
  
  try {
    const us = await Post.findByIdAndDelete(id);
    if(us){
      res.status(200).send({ msg: "Eliminacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al eliminar post"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"});
  }
}

async function getPost(req, res) {
  const { path } = req.params;

  try {
    const post = await Post.findOne({path});
    if(post){
      res.status(200).send( {msg: post});
    }else{
      res.status(400).send({ msg: "No se encontró ningún post"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"+err });
  }
  
}


function getPosts(req, res) {

  const { page=1, limit=10 } = req.query;

  const options = {
    page: parseInt(page),
    limit: parseInt(limit),
    sort: { created_at: "desc" }
  }

  Post.paginate({}, options, function(err, postStore) {
    if(err){
      res.status(400).send({msg: "No se ha encontrado ningún blog. "+err});
    }else{
      res.status(200).send(postStore);
    }
    
  });

}

module.exports = {
  createPost,
  updatePost,
  deletePost,
  getPost,
  getPosts
};