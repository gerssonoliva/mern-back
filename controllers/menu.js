const Menu = require("../models/menu");

async function createMenu(req, res){
  const menu = new Menu(req.body);

  menu.save().then((menuStore, err)=>{
    res.status(201).send(menuStore);
  }).catch((err)=>{
    res.status(400).send({msg: "Error al crear el usuario. "+err});
  })
}

async function getMenus(req, res) {

  const { active } = req.query;

  let response = null;

  if(active === undefined){
    response = await Menu.find().sort({ order: "asc" });
  }else{
    response = await Menu.find({ active });
  }

  if(response){
    res.status(200).send(response);
  }else{
    res.status(400).send({msg: "No se ha encontrado ningún menu"});
  }
}

async function updateMenu(req, res){
  const { id } = req.params;
  const menuData = req.body;

  try {
    const us = await Menu.findOneAndUpdate({ _id: id }, menuData);
    if(us){
      res.status(200).send({ msg: "Actualizacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al actualizar menu"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"});
  }
}

async function deleteMenu(req, res) {
  const { id } = req.params;
  
  try {
    const us = await Menu.findByIdAndDelete(id);
    if(us){
      res.status(200).send({ msg: "Eliminacion correcta"});
    }else{
      res.status(400).send({ msg: "Error al eliminar menu"});
    }
  } catch (err) {
    res.status(500).send({ msg: "Error de servidor"});
  }
}

module.exports = {
  createMenu,
  getMenus,
  updateMenu,
  deleteMenu
}