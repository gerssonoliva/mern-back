const bcrypt = require("bcryptjs");
// Import model
const User = require("../models/user");
const jwt = require("../utils/jwt");

function register(req, res) {

  const {firstname, lastname, email, password} = req.body;

  if(!email) res.status(400).send({msg: "El email es obligatorio"});
  if(!password) res.status(400).send({msg: "El password es obligatorio"});

  const user = new User({
    firstname,
    lastname,
    email: email.toLowerCase(),
    role: "user",
    active: false
  });

  const salt = bcrypt.genSaltSync(10); // Creacion de la encriptacion de password
  const hashPassword = bcrypt.hashSync(password, salt);

  user.password= hashPassword;

  user.save().then((err, userStore)=>{
    res.status(200).send(userStore);
  }).catch((err)=>{
    res.status(400).send({msg: "Error al crear el usuario. "+err});
  })

}

function login(req, res) {
  const {email, password} = req.body;

  if(!email) res.status(400).send({msg: "El email es obligatorio."});
  if(!password) res.status(400).send({msg: "El password es obligatorio."});

  const emailLowerCase = email.toLowerCase();

  User.find({email:emailLowerCase}).then((userStore, err) => {
    if (userStore) {
      bcrypt.compare(password, userStore.password, (check, err) => {
        if (err) {
          res.status(500).send({msg: "Error del servidor"});
        }else if(!check) {
          res.status(400).send({msg: "Contraseña incorrecta"});
        }else if(!userStore[0].active){
          res.status(401).send({msg: "Usuario no autorizado o no activo"});
        }else{
          res.status(200).send({
            access: jwt.createAccessToken(userStore[0]),
            refresh: jwt.createRefreshToken(userStore[0])
          });
        }
      })
    }
  }).catch((err) => {
    res.status(500).send({msg: "Error del servidor."});
  });
}

function refreshAccessToken(req, res) {
  const {token} = req.body;

  if(!token) res.status(400).send({msg: "Token requerido"});

  const {user_id} = jwt.decoded(token);

  User.find({ _id: user_id}).then((userStore, err) => {
    if(err){
      res.status(500).send({msg: "Error del servidor."});
    }else{
      res.status(200).send({
        accessToken: jwt.createAccessToken(userStore)
      });
    }
  }).catch((err) => {
    res.status(500).send({msg: "Error del servidor."});
  });

}

module.exports = {
  register,
  login,
  refreshAccessToken
}