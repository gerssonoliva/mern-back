function getFilePath(file) {
  const filePath = file.path; // uploads/avatar/[nombre_imagen]
  const fileSplit = filePath.split("\\"); // separar por /
  
  return `${fileSplit[1]}/${fileSplit[2]}`; // retornar todo menos la tabla uploads
}

module.exports = {
  getFilePath
}